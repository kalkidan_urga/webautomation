import com.sun.xml.internal.stream.util.ThreadLocalBufferAllocator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Gmail {
    public static void main(String[] args) throws InterruptedException, FileNotFoundException, UnsupportedEncodingException {
        int total = 0;
        PrintWriter writer = new PrintWriter("UnreadEmails.txt", "UTF-8");
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        WebDriver driver = new ChromeDriver();
//        driver.manage().window().maximize();
        String url = "https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession";
        driver.get(url);
        driver.findElement(By.id("identifierId")).sendKeys("kalkidanurga@gmail.com", Keys.ENTER);
        Thread.sleep(3000);
        driver.findElement(By.name("password")).sendKeys("5uM@KeBmgE", Keys.ENTER);
        Thread.sleep(4000);

        List<WebElement> unreademeil = driver.findElements(By.xpath("//*[@class='zF']"));
        for (int i = 0; i < unreademeil.size(); i++) {
            if (!unreademeil.get(i).getText().equals("")) {
                total += 1;
                writer.println("You have an unread email from: " + unreademeil.get(i).getText());
            }
        }

        writer.println("Total unread emails: " + total);
        writer.close();
        driver.close();

    }

}
